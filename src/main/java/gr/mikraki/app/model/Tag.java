package gr.mikraki.app.model;

 
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "test", type = "tag")
public class Tag {

	@Id
    private Long id;
     
    private String title;
    
    public Tag() {
		// TODO Auto-generated constructor stub
	}

	public Tag(Long id, String title) {
		super();
		this.id = id;
		this.title = title;
	}

	public Tag(String title) {
		super();
		id =1l;
		this.title = title;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
    
}
