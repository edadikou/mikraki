package gr.mikraki.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.SequenceGenerator;

@Entity
@NamedEntityGraph(name = Author.WITH_BOOKS ,attributeNodes = @NamedAttributeNode("books"))
@SequenceGenerator(name = "SEQ_ID", sequenceName = "author_seq", allocationSize = 1)
public class Author extends AbstractModel{

 	public static final String WITH_BOOKS = "Author.withBooks";

	private String name;
	
 	private Set<Book> books = new HashSet<Book>(0);
 

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@ManyToMany
	(fetch = FetchType.LAZY, mappedBy = "authors")
	public Set<Book> getBooks() {
		return books;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
	}
	
	
	
}
