package gr.mikraki.app.model;

public interface Item {
	
	public String getDescription();
	
	public Long getItemId();
	
	public double getPrice();
	
	public String getImg();
 
}
