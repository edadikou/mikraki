package gr.mikraki.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.springframework.data.elasticsearch.annotations.Document;

@Entity
@NamedEntityGraph(name =Book.WITH_AUTHORS ,attributeNodes = @NamedAttributeNode("authors"))
@SequenceGenerator(name = "SEQ_ID", sequenceName = "book_seq", allocationSize = 1)
@Document(indexName="books1",type="book")
public class Book extends AbstractModel implements Item{

	public static final String WITH_AUTHORS = "Book.withAuthors";
	
	private String isbn;
	
	private String isbn13;
	
	private String title;

	private String subtitle;

	private int pages;

	private double price;

	private String img;
	
	private String summary;

	private Set<Author> authors = new HashSet<Author>();
	 
	private Set<Category> categories = new HashSet<Category>();

	public Book() {
		super();
		// TODO Auto-generated constructor stub
	}


	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "book_author", joinColumns = {
					@JoinColumn(name = "BOOK_ID", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "AUTHOR_ID", nullable = false, updatable = false) })
	public Set<Author> getAuthors() {
		return authors;
	}
	
	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}
	
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "book_category", joinColumns = {
					@JoinColumn(name = "BOOK_ID", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "CATEGORY_ID", nullable = false, updatable = false) })
	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}
 
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}
	
	@org.springframework.data.annotation.Id
	public String getIsbn() {
		return isbn;
	}


	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}


	public String getIsbn13() {
		return isbn13;
	}


	public void setIsbn13(String isbn13) {
		this.isbn13 = isbn13;
	}


	@Override
	@Transient
	public String getDescription() {
		// TODO Auto-generated method stub
		return this.title;
	}

	@Override
	@Transient
	public Long getItemId() {
		// TODO Auto-generated method stub
		return this.id;
	}
	
	

	@Override
	public String toString() {
		return "Book [authors=" + authors + ", categories=" + categories + ", title=" + title + ", subtitle=" + subtitle
				+ ", pages=" + pages + ", price=" + price + ", img=" + img + "]";
	}
	
}
