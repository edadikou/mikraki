package gr.mikraki.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@NamedEntityGraph(name = Category.WITH_BOOKS,attributeNodes = @NamedAttributeNode("books"))
@SequenceGenerator(name="SEQ_ID",sequenceName="category_seq")
public class Category extends AbstractModel  {

	public static final String WITH_BOOKS = "Category.withBooks";

	private String description;
	 

	private Category parent;
	
	
	private Set<Category> childCategories = new HashSet<Category>();
	
 	private Set<Book> books = new HashSet<Book>(0);

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "categories")
	public Set<Book> getBooks() {
		return books;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	@ManyToOne
	@JoinColumn(name="parent_id")
	public Category getParent() {
		return parent;
	}

	public void setParent(Category parent) {
		this.parent = parent;
	}


	@OneToMany(fetch = FetchType.EAGER,mappedBy="parent")
	public Set<Category> getChildCategories() {
		return childCategories;
	}



	public void setChildCategories(Set<Category> childCategories) {
		this.childCategories = childCategories;
	}



	public void setBooks(Set<Book> books) {
		this.books = books;
	}



//	@Transient
//	public boolean isParent(){
//		return parent==null;
//	}
}
