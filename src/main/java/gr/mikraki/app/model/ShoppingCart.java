package gr.mikraki.app.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ShoppingCart {
	
	private final String SESSION_ID ;
	
	
	public ShoppingCart(String sessionID) {
		super();
		SESSION_ID = sessionID;
 	}

	/**
	 * 
	 * Map to store unique items of shopping cart.
	 * {@link Item#getItemId()} is used as key.
	 * 
	 */
	private Map<Long,ShoppingCartItem> shoppingCartItems = new HashMap<>();
	
 
	/**
	 * Adds item to shopping cart.
	 * 
	 * If the item is not already inserted in cart it creates an entry
	 * to shoppingCartItems map ,else it updates item;s quantity
	 * 
	 * @param item
	 */
	 
	public void addItem(Item item) {
	
		ShoppingCartItem cartItem = shoppingCartItems.get( item.getItemId() );
		if(cartItem !=null){
			cartItem.increaseQuantity(1);
			shoppingCartItems.put(cartItem.getItemId(), cartItem);

		}else{
			shoppingCartItems.put(item.getItemId(), new ShoppingCartItem(item, 1));
		}
 
	}
	
	
	
	public boolean adjustQuantity(Long itemId,int qty){
		
		ShoppingCartItem item = getItem(itemId);
		if(item!=null){
			item.setQuantity(qty);
			return true;
		} return false;
	}
	
	
	
	
	public ShoppingCartItem getItem(Long id){
		return shoppingCartItems.get(id);
	}

	public List<ShoppingCartItem> getItems() {
	
		return  new ArrayList<ShoppingCartItem>(shoppingCartItems.values());
				 
 	}
	
	public List<String> getItemIds() {
		return shoppingCartItems.keySet().stream().map(e->e.toString()).collect(Collectors.toList());
	}

 
	public void removeItem(Long itemId) {
		
		shoppingCartItems.remove(itemId);
	}



	public String getSESSION_ID() {
		return SESSION_ID; 
	}
	
	
}
