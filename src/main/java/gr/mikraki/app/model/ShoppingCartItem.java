package gr.mikraki.app.model;

public class ShoppingCartItem {

	private Item item;
	
 	private int quantity;
 	
 	private boolean isGift;
 	
  	
	public ShoppingCartItem(Item item, int quantity) {
		super();
		this.item = item;
		this.quantity = quantity;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public double getPrice(){
		return this.item.getPrice();
	}
	public Long getItemId(){
		return this.item.getItemId();
	}
	
	public String getDescription(){
		return this.item.getDescription();
	}
	
	public String getImg(){
		return this.item.getImg();
	}
	public int increaseQuantity(int i){
		
		quantity+=i;
		return quantity;
	}
	
	 public double getTotalPrice(){
		return getQuantity()*getPrice();
	}
	 
	public boolean isGift() {
		return isGift;
	}

	public void setGift(boolean isGift) {
		this.isGift = isGift;
	}

	@Override
	public String toString() {
		return "ShoppingCartItem [item=" + item.toString() + ", quantity=" + quantity + "]";
	}
	
	
}
