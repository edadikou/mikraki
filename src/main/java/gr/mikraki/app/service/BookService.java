package gr.mikraki.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gr.mikraki.app.jpa.repositories.BookRepository;
import gr.mikraki.app.model.Book;
import gr.mikraki.app.utils.Utils;

@Service
public class BookService extends AbstractService{
	
	@Autowired
	private BookRepository bookRepository;
 
	public List<Book> fetchForHomePage() {
		
 		return Utils.makeCollection(bookRepository.findAll());
	}

}
