package gr.mikraki.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import nz.net.ultraq.thymeleaf.LayoutDialect;

@EnableWebMvc // mvc:annotation-driven
@Configuration
@ComponentScan({ "gr.mikraki.app" })
@Import({ HibernateConfig.class })
public class RootConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/WEB-INF/resources/");
	}
 
	
	//TODO check SpringResourceTemplateResolver
	@Bean
	public ServletContextTemplateResolver templateResolver() {
		ServletContextTemplateResolver templateResolver = new ServletContextTemplateResolver();
		templateResolver.setPrefix("/WEB-INF/views/");
		templateResolver.setSuffix(".html");
		templateResolver.setCharacterEncoding("UTF-8");
		templateResolver.setCacheable(false);
		return templateResolver;
	}

	@Bean
	public SpringTemplateEngine templateEngine() {
		SpringTemplateEngine springTemplateEngine = new SpringTemplateEngine();
		springTemplateEngine.setTemplateResolver(templateResolver());

		 springTemplateEngine.addDialect(new LayoutDialect());
		return springTemplateEngine;
	}

 
	@Bean
	public ThymeleafViewResolver viewResolver() {
		ThymeleafViewResolver thymeleafViewResolver = new ThymeleafViewResolver();
		thymeleafViewResolver.setTemplateEngine(templateEngine());
		thymeleafViewResolver.setCharacterEncoding("UTF-8");
		thymeleafViewResolver.setContentType("text/html; charset=UTF-8");
		return thymeleafViewResolver;
	}
	
//	<bean id="messageSource" class="org.springframework.context.support.ResourceBundleMessageSource">
//	  <property name="basename" value="Messages" />
//	</bean>

	 @Bean
	 public ResourceBundleMessageSource messageSource(){
		 ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		 messageSource.setBasename("Messages");
		 return messageSource;
 	 }
	
}
