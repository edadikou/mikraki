//package gr.mikraki.app.config;
//
//import java.net.InetAddress;
//import java.net.UnknownHostException;
//
//import org.elasticsearch.client.Client;
//import org.elasticsearch.client.transport.TransportClient;
//import org.elasticsearch.common.transport.InetSocketTransportAddress;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
//import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
//import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
//
//@Configuration
//@EnableElasticsearchRepositories(basePackages = "gr.mikraki.app.elastic.repositories")
//public class ElasticConfig {
//
//	@Bean
//	public ElasticsearchTemplate elasticsearchTemplate() {
//		return new ElasticsearchTemplate(client());
//	}
//
//	@Bean
//	public Client client() {
//
//		Client client = null;
//		try {
//			client = TransportClient.builder().build()
//					.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("127.0.0.1"), 9300));
//		} catch (UnknownHostException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return client;
//	}
//
//}
