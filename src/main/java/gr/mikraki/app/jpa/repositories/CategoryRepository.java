package gr.mikraki.app.jpa.repositories;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.repository.CrudRepository;

import gr.mikraki.app.model.Category;
 
public interface CategoryRepository extends CrudRepository<Category, Long>{

	
	
	@EntityGraph(value = Category.WITH_BOOKS, type = EntityGraphType.LOAD)
	public Category getById(Long id);
	
	@Cacheable(value="category")
	public List<Category> findByParentIsNull();
	
	
	
//	public List<Category> findAllByOrderByParentCategoryDesc();
	
	
}
