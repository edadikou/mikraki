package gr.mikraki.app.jpa.repositories;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.repository.CrudRepository;

import gr.mikraki.app.model.Book;

 public interface BookRepository extends CrudRepository<Book, Long>{
 
	@EntityGraph(value = Book.WITH_AUTHORS, type = EntityGraphType.LOAD)
	public Book getById(Long id);
	
	public Book getByTitle(String title);
		  
 }
