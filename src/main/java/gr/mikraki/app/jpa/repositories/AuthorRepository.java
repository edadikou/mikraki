package gr.mikraki.app.jpa.repositories;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.repository.CrudRepository;

import gr.mikraki.app.model.Author;

public interface AuthorRepository extends CrudRepository<Author, Long>{
	
	@EntityGraph(value = Author.WITH_BOOKS, type = EntityGraphType.LOAD)
	public Author getById(Long id);
	
}
