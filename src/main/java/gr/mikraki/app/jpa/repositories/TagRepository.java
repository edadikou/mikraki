package gr.mikraki.app.jpa.repositories;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import gr.mikraki.app.model.Tag;

public interface TagRepository extends ElasticsearchRepository<Tag, Long>{
	
	 
	public Tag getById(Long id);
	public Tag findByTitle(String title);
	
}
