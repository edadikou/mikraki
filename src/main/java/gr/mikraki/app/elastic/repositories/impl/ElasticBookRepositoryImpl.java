//package gr.mikraki.app.elastic.repositories.impl;
//
//import java.util.List;
//
//import org.elasticsearch.index.query.QueryBuilder;
//import org.elasticsearch.index.query.QueryBuilders;
//import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
//import org.springframework.data.elasticsearch.core.query.SearchQuery;
//import org.springframework.stereotype.Repository;
//
//import gr.mikraki.app.elastic.model.ElasticBook;
//
//@Repository
//public class ElasticBookRepositoryImpl extends ElasticSearchRepository<ElasticBook> {
//
// 
//	public List<ElasticBook> searchBooks(String query){
//		SearchQuery searchQuery = new NativeSearchQueryBuilder()
//			    .withQuery(getSearchbookQuery(query))
// 			    .build();
//		return performQuery(searchQuery, ElasticBook.class);
//	}
//		
//	
//	private QueryBuilder getSearchbookQuery(String queryText) {
//		return QueryBuilders.multiMatchQuery(queryText, "title","subtitle") ;
//	}
//
// 
//
//}
