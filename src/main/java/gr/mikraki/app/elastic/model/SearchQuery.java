package gr.mikraki.app.elastic.model;

public class SearchQuery {
	
	String query;
	String type;
	
	
	public SearchQuery() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	

}
