package gr.mikraki.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import gr.mikraki.app.elastic.model.SearchQuery;
import gr.mikraki.app.jpa.repositories.CategoryRepository;
import gr.mikraki.app.service.BookService;

@Controller
@RequestMapping(path="/")
public class HomeController {
 
	@Autowired
	private BookService bookService;
 
	@Autowired 
	private CategoryRepository categoryRepository;
 

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView getHomeContent() {
		ModelAndView mav = new ModelAndView();
 		mav.addObject("books", 		bookService.fetchForHomePage());
 
  		mav.addObject("categories",	categoryRepository.findByParentIsNull());
 		mav.addObject("query",		 new SearchQuery());
 		mav.setViewName("home");
		return mav;
	}

}
