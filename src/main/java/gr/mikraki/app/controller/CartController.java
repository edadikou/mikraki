package gr.mikraki.app.controller;

import static gr.mikraki.app.utils.SessionConstants.ATTR_CART;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import gr.mikraki.app.model.Book;
import gr.mikraki.app.model.ShoppingCart;

@Controller
@RequestMapping(path="/cart")
@SessionAttributes({ATTR_CART})
public class CartController {
	
	private static Logger LOG = Logger.getLogger(CartController.class.getName());
	
	/**
	 * View names
	 */
	private static final String CART_DISPLAY = "shoppingCart";
	
	/**
	 * 
	 * Model attributes
	 * 
	 */
	private static final String SHOPPING_CART = "shoppingCart";
	

	/**
	 * Returns a shopping cart instance.
	 * 
	 * If shopping cart already exists in session returns the same,else
	 * it initializes a shopping cart,adds it to session and returns.
	 *   
	 * 
	 * @param httpSession
	 * @return shoppingCart
	 */
	@ModelAttribute
	public ShoppingCart getShoppingCart(HttpSession httpSession){
	
		ShoppingCart shoppingCart ;
		if((shoppingCart = (ShoppingCart) httpSession.getAttribute(ATTR_CART))== null){
			shoppingCart = new ShoppingCart(httpSession.getId()); 
 				LOG.debug("Shopping cart created "+shoppingCart.getSESSION_ID());
			 
		} 
		return shoppingCart;
	}
	
	 
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add(@ModelAttribute Book book, @ModelAttribute ShoppingCart shoppingCart,Model model) {
		
		shoppingCart.addItem(book);
		model.addAttribute(SHOPPING_CART,shoppingCart);
  		return String.valueOf( shoppingCart.getItems().size());
	}
	
	

	@RequestMapping(value = "/checkout", method = RequestMethod.POST)
	@ResponseBody
	public String checkout(@ModelAttribute ShoppingCart shoppingCart,Model model) {
		
		throw new NotYetImplementedException("Not Yet Here");
	}
	 
	
	
	@RequestMapping(method = RequestMethod.GET)
	public String displayCart(Model model,@ModelAttribute ShoppingCart shoppingCart) {
		
		model.addAttribute(SHOPPING_CART,shoppingCart);
 		return CART_DISPLAY;
	}
	
	
	
	@RequestMapping(path="/remove/{id}", method = RequestMethod.GET)
	public String remove(@PathVariable Long id,Model model,@ModelAttribute Book item,@ModelAttribute ShoppingCart shoppingCart) {
		
		shoppingCart.removeItem(id);
		model.addAttribute(SHOPPING_CART,shoppingCart);
 		return "redirect:/cart";
	}
	
	@ResponseBody
	@RequestMapping(value = "/update/{id}/{qty}", method = RequestMethod.GET)
	public String updateQuantity(@PathVariable Long id,@PathVariable int qty, @ModelAttribute ShoppingCart shoppingCart,Model model) {
		shoppingCart.adjustQuantity(id, qty);
		return String.valueOf( shoppingCart.getItem(id).getTotalPrice());
	}
	
}
