package gr.mikraki.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import gr.mikraki.app.jpa.repositories.AuthorRepository;

@Controller
@RequestMapping(path="/author")
public class AuthorController {
	
	@Autowired
	private AuthorRepository authorRepository;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String displayAuthor(@PathVariable Long id,Model model) {
 		model.addAttribute("author", authorRepository.getById(id));
	    return "author/display";
	}
	
}
