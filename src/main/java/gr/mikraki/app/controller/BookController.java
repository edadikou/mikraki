package gr.mikraki.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import gr.mikraki.app.jpa.repositories.BookRepository;
import gr.mikraki.app.model.Book;
 
@Controller
@RequestMapping("/book")
public class BookController {
	 

	@Autowired
	private BookRepository bookRepository;
	
	@RequestMapping(path = "/short/{id}", method = RequestMethod.GET)
	public String displayShortInfo(@PathVariable Long id,Model model) {
		Book b =  bookRepository.findOne(id);
		model.addAttribute("book", b);
	    return "book/display_min";
	}
	
	
	@RequestMapping(path  = "/{id}", method = RequestMethod.GET)
	public String displayFullInfo(@PathVariable Long id,Model model) {
		Book b =  bookRepository.getById(id);
 		model.addAttribute("book", b);
	    return "book/display";
	}
	
	
 
}
