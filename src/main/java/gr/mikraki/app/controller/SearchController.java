//package gr.mikraki.app.controller;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.servlet.ModelAndView;
//
//import gr.mikraki.app.elastic.model.SearchQuery;
//import gr.mikraki.app.jpa.repositories.CategoryRepository;
//
//@Controller
//@RequestMapping(path="/search")
//public class SearchController {
//	
//	@Autowired
//	private ElasticBookRepositoryImpl elasticBookRepo;
//	
//	@Autowired
//	private CategoryRepository categoryRepository;
//
//	@RequestMapping(method = RequestMethod.POST)
//	public ModelAndView search(@ModelAttribute SearchQuery searchQuery,ModelAndView mav) {
//		mav.addObject("categories",categoryRepository.findByParentIsNull());
// 		mav.addObject("books",  elasticBookRepo.searchBooks(searchQuery.getQuery()));
//		mav.addObject("query", new SearchQuery());
//		mav.setViewName("searchResults");
//		return mav;
//				
//	}
//	 
//}
