package gr.mikraki.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import gr.mikraki.app.jpa.repositories.CategoryRepository;
import gr.mikraki.app.model.Category;

@Controller
@RequestMapping(path="/category")
public class CategoryController {
	
 	@Autowired
	private CategoryRepository repo;
 
 	@RequestMapping(path  = "/{categoryId}", method = RequestMethod.GET)
	public String getRootCategory(@PathVariable Long categoryId,Model model) {
	    
 		Category c = repo.getById(categoryId);
  		model.addAttribute("category",c);
    	model.addAttribute("categories",repo.findByParentIsNull());

    	model.addAttribute("books",c.getBooks());
 		return"category/display";
	}
	
 

}
