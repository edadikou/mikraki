$(function () {
 
	setUpNotify() ;
	
	$('.cartForm').submit(function(event) {

 		event.preventDefault();
 		var form = $(event.target);
		var data = form.serialize();
  		
		$.ajax({
			type:  "POST",
 		    url: "http://localhost:8080/cart/add",
		    data: data,
			dataType : 'html',
			timeout : 5000,
			success : function(data) {
			 
			 
					$('#items').html(data);
					$.notify('Το προιον προστέθηκε στο καλάθι σας', {
						  style: 'happyblue',
						  autoHideDelay: 5000,
						  // show the arrow pointing at the element
						  arrowShow: true
						  
						});
				 
			},
			error : function(data) {
				console.log("ERROR");
				 
			} 
		});
	});
 
//	  $('#removeItemForm').submit( function() {
//
//	        $.ajax({
//	            url     : $(this).attr('http://localhost:8080/mikraki/cart/remove'),
//	            type    : $(this).attr('method'),
//	            data    : $(this).serialize(),
//	            success : function( response ) {
//	                        alert( response );
//	                      }
//	        });
//
//	        return false;
//	    });
//	 

	 

$(".increaseQty").click(function(e) {

		adjustPrice(e, 1);
});

$(".decreaseQty").click(function(e) {

		adjustPrice(e, -1);

});


function adjustPrice(e,df){
	
	item = $(e.target).parents(".row");
 	qty = parseInt(item.find("#qty").text());
 	id = parseInt(item.find("#itemID").val());
 	
 	if(qty+df>0){ 
 		
 		qty+=df;
 		price *= qty;
		
 		$.ajax({
			type:  "GET",
 		    url: "http://localhost:8080/cart/update/"+id+"/"+qty,
			timeout : 5000,
			success : function(data) {
		
				 (item.find("#totalPrice").text(parseFloat(data).toFixed(2)));
				item.find("#qty").text(qty);

				 
			},
			error : function(data) {
				console.log("ERROR");
				 
			} 
		});
	}
}

function setUpNotify(){
	
$.notify.addStyle('happyblue', {
		  html: "<div>" +
		  		"	<div data-notify-text/>" +
		  		"	<a  href='/cart' class='cart-link' role='button' >Μεταβαση στο καλαθι</a>"+
		  		"</div>",
		  classes: {
		    base: {
		    "background-color": "#d4d4d4",
		    "background-image": "url('http://findicons.com/files/icons/1579/devine/256/cart.png')",
		    "background-repeat":" no-repeat",
		    "background-size": "60px 60px",
		    "border-radius": "10px",
		    "border-right": "medium solid grey",
		    "border-top": "medium solid grey",
		    "padding": "15px 15px 15px 60px",
		    "white-space":" nowrap"
		     
		    } 
		  }
		});
	console.log("finished");
	
}
 
});

 
